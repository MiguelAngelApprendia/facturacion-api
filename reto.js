//var admin = require("firebase-admin");
//var serviceAccount = require("./config/service_accounts/admin-dev.json");

//var refTim = require(global.rootPath("./workers/timbrar.js"));

var dbAdmin = require(global.rootPath("./config/admin.js"));
/*admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://reto1-facturacion.firebaseio.com"
});*/
var db = dbAdmin.db;
var ref = db.ref("timbrarRequests");

//var arrayFacturas = ["-LAj1GaJNP9Dh1cbuj94","-LDNghpSGyrEAuJNeyPs","-LDE46qpxbhbPiq-584k"];

module.exports = function(){
    return function(arrayFacturas){
        return timbrarFacturas(arrayFacturas)
        .then((message) => {
            console.log(message);
        })
        .catch((err) => {
            console.log(err);
        });
    }
}();


function timbrarUnaFactura(facturas) {
    return new Promise((resolve, reject) => {
        if (arrayFacturas.length == 1) {
            ref.orderByChild("factura").equalTo(arrayFacturas[0]).limitToLast(1).once("value", (snapshot) => {
                snapshot.forEach((datos) => {
                    if (datos.val().status == 3 && datos.val().factura == datos.val().factura) {
                        refTim.onTimbrarRequestAdded(datos);
                        console.log("ID : " + datos.key
                            + "\nDATOS FACTURA\nACCOUNT :"
                            + datos.val().account +
                            "\nFACTURA: " + datos.val().factura + "\nSTATUS: " + datos.val().status + "\n");
                        resolve();
                    }
                })
            });
        }
    })
}

var r = 0;
function timbrarFacturas(facturas) {
    return new Promise((resolve, reject) => {
        var arrayFacturasError = [];
        if (facturas.length > 1) {
            ref.orderByChild("factura").once("value", (snapshot) => {
                var i = 1;
                arrayFacturas.forEach((fact) => {
                    do {
                        //console.log("while")
                            ref.orderByChild("factura").equalTo(fact).limitToLast(1).once("child_added", (snap) => {  
                                //console.log(snap.val().factura)
                                if (snap.val().status == 3 && snap.val().factura == fact) {
                                    //console.log("aqui");
                                    refTim.onTimbrarRequestAdded(snap);
                                    console.log("ID : " + snap.key
                                        + "\nDATOS FACTURA\nACCOUNT :"
                                        + snap.val().account +
                                        "\nFACTURA: " + snap.val().factura + "\nSTATUS: " + snap.val().status+"\n")
                                    i++;
                                    
                                    resolve("\n---->>>  Se mandaron a timbrar las Facturas  <<<-----\n");
                                }
                        })
                        r = 1;
                    }while(r == 0);
                })
            })
            //console.log("Se timbraron todas las facturas");
        } else if (facturas.length == 1) {
            timbrarUnaFactura(facturas).
                then(() => {
                    //console.log("factura timbrada");
                    resolve("Se mando a timbrar la factura");
                })
            //.catch((error) => {
            //  console.log(error);
            //})
        } else {
            reject("No se encontraron facturas");
        }
    }
    )
}



