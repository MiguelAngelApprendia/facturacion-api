process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

module.exports = function () {
	return function (factura) {
		let libxml = require('libxslt').libxmljs
		let forge = require('node-forge')
		let moment = require('moment-timezone')

		// Constants
		let { usr, pwd } = require('./constants')


		let args = {
			user: usr,
			password: pwd,
			rfc: factura.emisor.rfc,
			uuid: factura.uuid,
		}

		let connection = require('./connection')

		return connection.connect().then(function (client) {
			return client['getCfdiFromUUIDAsync'](args)
		}).then(function (result) {
			if (!result) { throw 'getCfdiFromUUID No result' }

			let response = result['getCfdiFromUUIDReturn']
			let zip = new require('node-zip')(response, {
				base64: true,
				checkCRC32: true
			})

			let comprobante = zip.files[`${factura.uuid}_.xml`]
			let resultDoc = libxml.parseXmlString(comprobante.asText(), {
				noblanks: true
			})


			let timbre = resultDoc.get('//tfd:TimbreFiscalDigital', {
				tfd: 'http://www.sat.gob.mx/TimbreFiscalDigital'
			})

			return {
				doc: resultDoc,
				timbre: timbre
			}
		}).catch(function (e) { return Promise.reject(e) })
	}
}()
