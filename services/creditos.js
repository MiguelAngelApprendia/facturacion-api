module.exports = function () {
	return {
		current: getLastCredito
	}

	function getLastCredito(accountId) {
		let db = require(global.rootPath("config/admin")).db;
		let lastCredito = db.ref(`accounts/${accountId}/creditos`).limitToLast(1)

		return lastCredito.once('value').then(function (snap) {
			if (!snap.val()) { return null }
			let id = Object.keys(snap.val())[0]

			return snap.ref.child(id).once('value').then(async function (snap) {
				if (!snap.val()) { return null }

				return {
					count: await getFacturasCountFor(snap)
					use: assignFacturaToCredito.bind(snap)
				}
			})
		})
	}


	function getFacturasCountFor(creditoSnap) {
		debugger
		let facturas = creditoSnap.ref
			.parent().parent()
			.child('facturas')
			.orderByChild('credito')
			.equalTo(creditoSnap.key)

		return facturas.once('value').then(function(snap){
			return snap.numChildren()
		})
	}


	async function assignFacturaToCredito(facturaSnap){
		// this = creditoSnap
		if(!this){ return false }

		return await facturaSnap.ref.update({credito: this.key})
			.then(()=>{ return true })
			.catch(()=>{ return false })
	}

}()
