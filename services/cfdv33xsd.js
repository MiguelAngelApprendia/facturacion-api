module.exports = function () {
	let fs = require('fs')
	let chdir = require('in-folder')
	let Promise = require('promise')
	let libxsd = require('libxml-xsd')

	const satPath = 'config/zfactu/sat'

	return new Promise(function (resolve, reject) {
		try {
			chdir(global.rootPath(satPath), function () {
				return new Promise(function(resolve, reject){
					let xsd = fs.readFileSync('cfdv33.xsd', 'utf8')
					libxsd.parse(xsd, function (err, schema) {
						if (err) { return reject(err) }
						resolve(schema);
					})
				}).then(resolve).catch(reject)
			})
		} catch (e) {
			reject(e)
		}
	})
}()
