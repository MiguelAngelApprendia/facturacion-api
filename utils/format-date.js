module.exports = function(){
	let tz = 'America/Mexico_City'

	function local(date, format){
		let moment = require('moment-timezone')
		format = format || 'YYYY-MM-DDTHH:mm:ss'

		moment.locale('es')

		return moment(date).tz(tz).format(format)
	}

	return {
		local: local
	}
}()
